package project.riteh.keyboardnks;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

import java.util.List;

public class SimpleIME extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    List<Keyboard.Key> keys;
    private KeyboardView kv;
    private Keyboard keyboard;
    private boolean caps = false;
    private boolean isRunning = false;
    StringBuilder dotPeriod = new StringBuilder();
    long time;
    long timePressed;
    int idKeyCode = -1000;
    CountDownTimer countWord = new CountDownTimer(2000, 1000) {
        public void onTick(long millisUntilFinished) {

        }
        public void onFinish() {
            getCode(1);
        }
    };

    CountDownTimer countLetter = new CountDownTimer(1200, 400) {
        public void onTick(long millisUntilFinished) {
            isRunning = true;
            Log.d("COUNTER", "COUNTER: "+millisUntilFinished);
        }
        public void onFinish() {
            getCode(0);
            dotPeriod = new StringBuilder();
            isRunning = false;
        }
    };

    @Override
    public View onCreateInputView() {
        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard, null);
        kv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (idKeyCode == 101) {
                        Log.d("ACTION UP", "ACTION UP");
                        time = System.currentTimeMillis() - timePressed;
                        countWord.cancel();
                        countLetter.start();
                        makeCode();
                        Log.d("DOTPERIOD", "DOTPERIOD: " + dotPeriod);
                        idKeyCode = -1000;
                    }
                }
                return false;
            }
        });
        keyboard = new Keyboard(this, R.xml.keys);
        kv.setKeyboard(keyboard);
        kv.setOnKeyboardActionListener(this);
        keys = keyboard.getKeys();
        return kv;
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        // TO DO
    }

    @Override
    public void onPress(int primaryCode) {
        kv.setPreviewEnabled(false);
        idKeyCode = primaryCode;
        Log.d("ACTION DOWN", "ACTION DOWN: "+idKeyCode);
        if (idKeyCode == 101) {
            countWord.start();
            timePressed = System.currentTimeMillis();
            if (isRunning) {
                countLetter.cancel();
                isRunning = false;
            }
        }
    }

    @Override
    public void onRelease(int primaryCode) {
        kv.setPreviewEnabled(false);
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
        InputConnection ic = getCurrentInputConnection();
    }

    @Override
    public void swipeLeft() {
        InputConnection ic = getCurrentInputConnection();
        ic.deleteSurroundingText(1, 0);
        countWord.cancel();
        countLetter.cancel();
        dotPeriod = new StringBuilder();
    }

    @Override
    public void swipeRight() {
        InputConnection ic = getCurrentInputConnection();
        ic.commitText(" ",1);
        countWord.cancel();
        countLetter.cancel();
        dotPeriod = new StringBuilder();
    }

    @Override
    public void swipeUp() {
        InputConnection ic = getCurrentInputConnection();

    }

    private void makeCode(){
        if (time > 1500){
            //SPACE
        }else if (time >= 400){
            dotPeriod.append('_');
        }else if(time < 400){
            dotPeriod.append('.');
        }
    }

    private void getCode(int i){
        InputConnection ic = getCurrentInputConnection();
        String commit = dotPeriod.toString();
        if (i == 1){
            ic.commitText(" ",1);
        }else if (commit.equals("._")){
            int code = 97;//A
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("_...")){
            int code = 98;//B
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("_._.")){
            int code = 99; //C
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_..")){
            int code = 100; //D
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".")){
            int code = 101; //E
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".._.")){
            int code = 102; //F
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("__.")){
            int code = 103;//G
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("....")){
            int code = 104;//H
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("..")){
            int code = 105;//I
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".___")){
            int code = 106;//J
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_._")){
            int code = 107;//K
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("._..")){
            int code = 108; //L
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("__")){
            int code = 109;//M
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_.")){
            int code = 110;//N
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("___")){
            int code = 111;//O
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".__.")){
            int code = 112; //P
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("__._")){
            int code = 113; //Q
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("._.")){
            int code = 114;//R
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("...")){
            int code = 115; //S
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_")){
            int code = 116;//T
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".._")){
            int code = 117;
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("..._")){
            int code = 118;//V
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".__")){
            int code = 119;//W
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_.._")){
            int code = 120;//X
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_.__")){
            int code = 121; //Y
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("__..")){
            int code = 122; //Z
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_____")){
            int code = 48;//0
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".____")){
            int code = 49;//1
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("..___")){
            int code = 50;//2
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("...__")){
            int code = 51;//3
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("...._")){
            int code = 52;//4
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals(".....")){
            int code = 53;//5
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("_....")){
            int code = 54;//6
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("__...")){
            int code = 55;//7
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("___..")){
            int code = 56;//8
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("____.")){
            int code = 57;//9
            ic.commitText(String.valueOf((char)code),1);
        }else if (commit.equals("._._._")){
            int code = 46;//Full-Stop .
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("__..__")){
            int code = 44;//Comma
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("___...")){
            int code = 58;//Colon :
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("..__..")){
            int code = 63;//Question Mark ?
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("_.._.")){
            int code = 47;//Slash /
            ic.commitText(String.valueOf((char) code), 1);
        }else if (commit.equals("_...._")){
            ic.commitText("-", 1);//Hyphen -
        }else if (commit.equals(".__._.")){
            ic.commitText("@", 1);//At sign @
        }else if (commit.equals("_..._")){
            ic.commitText("=", 1);//Equals sign =
        }else if (commit.equals("_._.__")){
            ic.commitText("!", 1);//Exclamation mark !
        }else if (commit.equals(".____. ")){
            ic.commitText("'", 1);//Apostrophe '
        }else if (commit.equals("........")){
            ic.deleteSurroundingText(1, 0);
        }else{
            Log.d("OUT", "Not matchable code");
        }
    }
}
